﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ken : MonoBehaviour
{
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        transform.Translate(h * Time.deltaTime * 5, 0, 0);
        anim.SetFloat("speed", h);

        if (Input.GetKeyUp(KeyCode.Space)) {

            anim.SetTrigger("hadouken");
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {

        print("THIS IS PRETTY MUCH THE SAME AS IN 3D");
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("TRIGGER ALSO WORKS");
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
    }
}
